# Образ для запуска сборок в CI с CLI Яндекс.Облака

- [Официальная инструкция Yandex Cloud CLI](https://cloud.yandex.ru/docs/cli/quickstart)
- [Инструкция по облачному развертыванию UniEnv](https://uenv.ru/help/cloud)

## Сборка

```bash
docker build -t atnartur/yc:latest .
docker push atnartur/yc:latest
```